import sys
import numpy as np
#from skimage.io import imread
import io

from PIL import Image

from pypylon import pylon


name='Top'

tl_factory = pylon.TlFactory.GetInstance()
ptl = tl_factory.CreateTl('BaslerGigE')
empty_camera_info = ptl.CreateDeviceInfo()

if name=='OnAxis':
    ip = '172.16.127.16'
elif name=='Top':
    ip = '172.16.127.17'
else:
    raise Exception('Basler name does not exist. Use OnAxis or Top')

empty_camera_info.SetPropertyValue('IpAddress', ip)
camera_device = tl_factory.CreateDevice(empty_camera_info)
camera = pylon.InstantCamera(camera_device)
camera.Open()
camera.StartGrabbingMax(1)

result = camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)
if result.GrabSucceeded():
    image = result.Array
    path = "/data/visitors/nanomax/common/sw/baslerLiveViewer/baslerliveviewer/"
    Image.fromarray(image).save(path+"basler_full.png")
    Image.fromarray(image[:,:,0]).save(path+"basler_0.png")
    Image.fromarray(image[:,:,1]).save(path+"basler_1.png")

    print("Saved images")
    #return image[:,:,1]
else:
    print("Did not work")

print(camera.PixelFormat.Symbolics)
print ("camera.PixelFormat.GetValue: ", camera.PixelFormat.GetValue())

result.Release()
camera.StopGrabbing()
camera.Close()
