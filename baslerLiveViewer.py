"""
A live viewer app which grabs data from the pylon interface
and plots them in a silx widget.
"""

import sys
import numpy as np
from silx.gui.plot import ImageView, PlotWindow, Plot2D, tools
from silx.gui import qt
from silx.gui.plot.PlotWidget import items
#import zmq
#from zmq.utils import jsonapi as json
import requests
import time
import io

from PIL import Image
import os
from pypylon import pylon

##########################################################################################

class LiveViewer2dPlotBase(Plot2D):
    """
    Displays 2d detector images live from the Basler microscopes.

    """

    def __init__(self, baslerMic, interval=.1):
        # run the base class constructor
        super().__init__()

        # initialize the connection
        self.initialize(baslerMic)

        # set some properties
        self.setWindowTitle("BaslerLiveViewer ("+baslerMic+")")
        self.setKeepDataAspectRatio(True)
        self.setYAxisInverted(True)
        self.hasImage = False
        self.waiting_for_frame = False
        self.latest_request = time.time()

        #Initial image for proper zoom and failed grabbing
        self.img_sub = self._get_random_image()
        self.addImage(self.img_sub,replace=True,copy=False,resetzoom=True)

        # a periodic timer triggers the update
        self.timer = qt.QTimer(self)
        self.timer.setInterval(int(interval * 1000.0))
        self.timer.timeout.connect(lambda: self._update())
        self.timer.start()

    def initialize(self):
        raise NotImplementedError

    def _get_image(self):
        """
        gets the last image
        """
        raise NotImplementedError

    def _update(self):
        image_0 = self._get_basler_image()
        #image = self._get_random_image() #Uncomment for testing without microscope
        image = self._YUV422toRGB(image_0)
        if image is None:
            return
        self.addImage(image,replace=True,copy=False,resetzoom=False)
        self.hasImage = True

    def _YUV422toRGB(self,image):
        #Function to convert YUV422 to RGB data
        Y = image[:,:,1]
        U = np.kron(image[:,0::2,0], np.ones((1,2)))
        V = np.kron(image[:,1::2,0], np.ones((1,2)))

        R = np.rint(1.164*(Y-16) + 1.793*(V-128))
        G = np.rint(1.164*(Y-16) - 0.534*(V-128) - 0.213*(U-128))
        B = np.rint(1.164*(Y-16) + 2.115*(U-128))
        
        conv_image = np.zeros((image.shape[0],image.shape[1],3))
        conv_image[:,:,0] = R
        conv_image[:,:,1] = G
        conv_image[:,:,2] = B

        #np.where seems to need the inverse condition
        conv_image = np.where(conv_image > 0, conv_image, 0)
        conv_image = np.where(conv_image < 255, conv_image, 255)
        conv_image = conv_image/255
        return conv_image

    def closeEvent(self,event):
        self._cameraClose()
        event.accept()
        #sys.exit() #Comment to keep 1 window open in double viewer

class BaslerLiveViewer(LiveViewer2dPlotBase):
    def initialize(self,baslerMic):
        #self.session = requests.Session()
        #self.session.trust_env = False
        self.setBackgroundColor(qt.QColor(255, 255, 255))
        self.setGraphTitle('Basler '+baslerMic)
        self.baslerMic = baslerMic

        #Initialize the Basler camera object
        self.camera = self._cameraInit(baslerMic)
        #Check if marker positions have been saved
        markerData = self._readMarkerPositions()
        #Set markers by default or reading file
        self._createMarkers(markerData)

    def _cameraInit(self,name):
        #Create the camera object
        tl_factory = pylon.TlFactory.GetInstance()
        ptl = tl_factory.CreateTl('BaslerGigE')
        empty_camera_info = ptl.CreateDeviceInfo()

        if name=='OnAxis':
            ip = '172.16.127.16'
        elif name=='Top':
            ip = '172.16.127.17'
        elif name=='Nimis':
            ip = '172.16.127.14'
        else:
            raise Exception('Basler name does not exist. Use OnAxis, Top or Both')

        empty_camera_info.SetPropertyValue('IpAddress', ip)
        camera_device = tl_factory.CreateDevice(empty_camera_info)
        camera = pylon.InstantCamera(camera_device)
        camera.Open()
        camera.StartGrabbing()

        return camera

    def _get_random_image(self):
        #Get random images for code testing without microscope access
        image_r = np.ones((1024,1280,3))
        return image_r

    def _get_basler_image(self):
        #Get the last image acquired by the mciroscope
        result = self.camera.RetrieveResult(5000,pylon.GrabStrategy_LatestImageOnly)
        if result.GrabSucceeded():
            image = result.Array
            self.img_sub = image
            result.Release()
            return image
        else:
            #If process fails, return last image
            return self.img_sub

    def _cameraClose(self):
        print("Closing camera and saving marker positions")
        self._saveMarkerPositions()
        self.camera.StopGrabbing()
        self.camera.Close()

    def _saveMarkerPositions(self):
        #Save marker coordinates in a hidden txt to remember for next time
        items_plot = self._getItems()

        file = open(".markerPositions_"+self.baslerMic+".txt",'w')
        file.write("Marker legends, positions and text for "+self.baslerMic+"\n")
        file.close()

        with open(".markerPositions_"+self.baslerMic+".txt","a") as file:
            for item in items_plot:
                if isinstance(item, items.MarkerBase):
                    markerName = item.getLegend()
                    file.write(markerName+"\n")
                    markerPos  = item.getPosition()
                    file.write(str(markerPos[0])+","+str(markerPos[1])+"\n")
                    markerText = item.getText()
                    file.write(markerText+"\n")

    def _readMarkerPositions(self):
        #Read marker positions for initialization
        filename = ".markerPositions_"+self.baslerMic+".txt"
        if os.path.exists(filename):
            with open(filename, "r") as file:
                next(file) #Skip header
                markerData = file.readlines()
        else:
            markerData = -1

        return markerData

    def _createMarkers(self,markerData):
        #Generate markers by default or from file
        if markerData == -1:
            self.addMarker(50, 50, legend='OAM12', text='12', color='y', selectable=False, draggable=True, symbol='+', constraint=None)
            self.addMarker(50, 100, legend='OAM0', text='0', color='y', selectable=False, draggable=True, symbol='+', constraint=None)
        else:
            numMarkers = int(len(markerData) / 3)
            for i in range(numMarkers):
                legend    = markerData[i*3].split("\n")
                positions = list(markerData[i*3+1].split(","))
                text      = markerData[i*3+2].split("\n")
                self.addMarker(positions[0],positions[1],legend=legend[0],text=text[0],color='y',selectable=False, draggable=True, symbol='+', constraint=None)
        
##########################################################################################

if __name__ == '__main__':
    # you always need a qt app     
    app = qt.QApplication(sys.argv)
    app.setStyle('Fusion')

    # parse arguments
    if len(sys.argv) < 2:
        raise Exception('Basler name not provided. Use OnAxis, Top or Both')
    else:
        baslerMic = sys.argv[1]

    interval=0.1 #Hardcoded for the moment

    if baslerMic == 'Both':
        #In the future, both viewers in same main window
        viewerTop = BaslerLiveViewer("Top", interval=interval)
        viewerOnAxis = BaslerLiveViewer("OnAxis", interval=interval)
        viewerTop.show()
        viewerOnAxis.show()
    else:
        # instantiate the viewer and run
        viewer = BaslerLiveViewer(baslerMic, interval=interval)
        viewer.show()

    app.exec_()

#########################################################################################
